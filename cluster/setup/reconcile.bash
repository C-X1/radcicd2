namespace=dev
release=keycloak

kubectl delete statefulset -l app.kubernetes.io/instance=$release
kubectl delete pvc -l app.kubernetes.io/instance=$release
kubectl delete pod -l app.kubernetes.io/instance=$release

flux reconcile source git flux-system
flux reconcile kustomization flux-system

flux suspend helmrelease $release
flux resume helmrelease $release

flux reconcile helmrelease --namespace $namespace $release
