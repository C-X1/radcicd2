"""
  Steps for var_manip ansible module
"""
# pylint: disable=function-redefined, missing-function-docstring

from behave import use_step_matcher, when, given, then # pylint: disable=no-name-in-module
import var_manip
import yaml
import importlib
import traceback

use_step_matcher("re")

@given('the variable "(?P<variable>.*)" is set to (?P<isyaml>yaml )?"(?P<value>.*)"')
def step_impl(context, variable, isyaml, value):
    if "var" not in context:
        context.var = {}

    if isyaml:
        value = yaml.safe_load(value)

    context.var[variable] = value

@when('the var_manip entry parser is run')
def step_impl(context):
    try:
        entry_keys = var_manip.EntryParser().parse(context.var["entry"])
        context.keys = []
        context.index_flags = []
        if entry_keys:
            context.index_flags, context.keys = map(list, zip(*entry_keys))

    except Exception as exception: # pylint: disable=broad-except
        context.exception = exception
        context.traceback = traceback.format_exc()

@when('the var_manip.get_variable_entry function is run')
def step_impl(context):
    init_value = context.var.get("init_value")
    entry = context.var.get("entry")
    context.result = var_manip.get_variable_entry(init_value, entry)

@when('the var_manip.update_variable_entry function is run')
def step_impl(context):
    try:
        variable = context.var.get("init_value")
        value = context.var.get("set_value")
        entry = context.var.get("entry")
        var_type = context.var.get("var_type")
        force = context.var.get("force") == "True"
        create = context.var.get("create") == "True"
        action = context.var.get("action")

        var_manip.update_variable_entry(entry, variable, action, value, var_type, create, force)
    except Exception as exception: # pylint: disable=broad-except
        context.exception = exception
        context.traceback = traceback.format_exc()

@then('the result should be "(?P<result>.*)"')
def step_impl(context, result):
    assert "result" in context
    assert str(context.result) == result, "{} != {}".format(context.result, result)

@then('the index flags should be "(?P<index_flags>.*)"')
def step_impl(context, index_flags):
    assert "index_flags" in context
    context_flags = ""

    for context_index_flag in context.index_flags:
        flag = "0"
        if context_index_flag:
            flag = "1"
        context_flags += flag

    assert context_flags == index_flags, "{} != {}".format(context_flags, index_flags)

@then('the keys should be "(?P<keys>.*)"')
def step_impl(context, keys):
    assert "keys" in context
    context_keys_str = ""
    if context.keys:
        context_keys_str_p = [str(key) for key in context.keys]
        context_keys_str = ",".join(context_keys_str_p)
    assert context_keys_str == keys, "'{}' != '{}'".format(context_keys_str, keys)

@then('(?P<exception>.*) exception should be raised')
def step_impl(context, exception):
    passed = False
    exception = exception.replace('"', '')
    assert exception != ""
    if exception.lower() == "no":
        passed = ("exception" not in context)
    else:
        assert "exception" in context
        #We need a string comparison here.
        passed = context.exception.__class__.__name__ == exception

    assert passed, "Unexpeced Exception: \
    {}: {}\n {}".format(context.exception.__class__.__name__,
                        context.exception,
                        context.traceback)
