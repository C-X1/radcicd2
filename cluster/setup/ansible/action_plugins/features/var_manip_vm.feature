Feature: Variable Manipulator Class


  Scenario Outline: Testing setting entry from empty dict
    Given the variable "init_value" is set to yaml "{}"
    And the variable "entry" is set to "<entry>"
    And the variable "action" is set to "<action>"
    And the variable "set_value" is set to "<value>"
    And the variable "create" is set to "<create>"
    And the variable "force" is set to "<value>"
    And the variable "var_type" is set to "<var_type>"

    When the var_manip.update_variable_entry function is run

    When the var_manip.get_variable_entry function is run
    Then No exception should be raised
    And the result should be "<check_value>"

    Examples: Test to set variable
      | entry              | value | check_value | action | var_type | create | force |
      | postgres.root.pw   | narf  | narf        | set    | None     | True   | False |
      | postgres.root.user | narf  | narf        | set    | None     | True   | False |


  Scenario Outline: Testing getting entry from variable
    Given the variable "init_value" is set to yaml "{a: 1, b: {c: 3, d: 4, x: [1,2,[3,4]]}, e: 5, fgh: [6, 7, 8]}"
    And the variable "entry" is set to "<entry>"
    When the var_manip.get_variable_entry function is run
    Then the result should be "<result>"
    
    Examples: Lookuptests: <comment>
      | comment             | entry     | result |
      | Get a               | a         |      1 |
      | Get b.c             | b.c       |      3 |
      | Get b.d             | b.d       |      4 |
      | Get e               | e         |      5 |
      | Get fgh[0]          | fgh[0]    |      6 |
      | Get fgh[1]          | fgh[1]    |      7 |
      | Get fgh[2]          | fgh[2]    |      8 |
      | Get fgh[3] (N/A)    | fgh[3]    |   None |
      | z does not exist    | z         |   None |
      | Dict Array in Array | b.x[2][0] |      3 |
      | Get a.b             | a.b       |   None |



  Scenario Outline: Testing setting entry from variable
    Given the variable "init_value" is set to yaml "{a: 1, b: {c: 3, d: 4, x: [1,2,[3,4]]}, e: 5, fgh: [6, 7, 8]}"
    And the variable "entry" is set to "<entry>"
    And the variable "action" is set to "<action>"
    And the variable "set_value" is set to "<value>"
    And the variable "create" is set to "<create>"
    And the variable "force" is set to "<value>"
    And the variable "var_type" is set to "<var_type>"

    When the var_manip.update_variable_entry function is run

    Given the variable "entry" is set to "<check_entry>"
    When the var_manip.get_variable_entry function is run
    Then No exception should be raised
    And the result should be "<check_value>"

    Examples: Test to set variable
      | entry     | check_entry | value   | check_value | action       | var_type | create | force |
      | a         | a           | a       | a           | set          | None     | False  | False |
      | b.c       | b.c         | b       | b           | set          | None     | False  | False |
      | b.d       | b.d         | c       | c           | set          | None     | False  | False |
      | e         | e           | d       | d           | set          | None     | False  | False |
      | fgh[0]    | fgh[0]      | e       | e           | set          | None     | False  | False |
      | fgh[1]    | fgh[1]      | f       | f           | set          | None     | False  | False |
      | fgh[2]    | fgh[2]      | g       | g           | set          | None     | False  | False |
      | b.x[2][0] | b.x[2][0]   | h       | h           | set          | None     | False  | False |
      | fgh       | fgh[3]      | i       | i           | append       | None     | False  | False |
      | b.x[2]    | b.x[2][2]   | j       | j           | append       | None     | False  | False |
      | fgh       | fgh[1]      | 7       | 8           | remove       | int      | False  | False |
      | b.x[2]    | b.x[2][0]   | 3       | 4           | remove       | int      | False  | False |
      | b.x[2]    | b.x[2][4]   | [2,3,4] | 4           | append_array | list     | False  | False |
      | b.x       | b.x[0]      | 0       | 2           | pop          | None     | False  | False |
      | z.g.h     | z.g.h       | 8       | 8           | set          | None     | True   | False |


  Scenario Outline: Testing setting exception cases
    Given the variable "init_value" is set to yaml "{a: 1, b: {c: 3, d: 4, x: [1,2,[3,4]]}, e: 5, fgh: [6, 7, 8]}"
    And the variable "entry" is set to "<entry>"
    And the variable "action" is set to "<action>"
    And the variable "set_value" is set to "<value>"
    And the variable "create" is set to "<create>"
    And the variable "force" is set to "<force>"
    And the variable "var_type" is set to "<var_type>"

    When the var_manip.update_variable_entry function is run
    Then "<exception>" exception should be raised
    
    Examples: Test to set variable 
      | entry   | value | action | var_type | create | force | exception |
      | a.b     | a     | set    | None     | False  | False | KeyError  |
      | x[2][3] | a     | set    | None     | True   | True  | TypeError |

