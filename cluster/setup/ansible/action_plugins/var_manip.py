# Copyright: (c) 2020, Christian Holl <cyborgx1@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.module_utils.six import iteritems, string_types
from ansible.module_utils.parsing.convert_bool import boolean
from ansible.plugins.action import ActionBase
from ansible.utils.vars import isidentifier

import ansible.constants as C

class EntryParser():
    """
    Class which parses the entry parameter of the module
    """

    def get_entry_separators(self, entry): # pylint: disable=no-self-use
        """
        Returns all non escaped separators
        """
        separators = []
        escaped = False
        #Find active separators
        if entry:
            for num, char in enumerate(entry):
                if char == "\\":
                    escaped = not escaped
                else:
                    if not escaped:
                        if char in ".[]":
                            separators.append((num, char))
                    escaped = False


            if not separators or separators[-1][0] < len(entry) - 1:
                separators.append((len(entry), "."))

            if entry[-1] in "[.":
                raise SyntaxError("Unexpected {} at the end of entry parameter".format(entry[-1]))
        return separators

    def parse(self, entry):
        """
        Evaluates the entry string
        Returns the entry keys of the entry parameter
        in the form of a tuple (is_array, index_or_key)
        """

        separators = self.get_entry_separators(entry)

        #Check and extract
        last_separator = (-1, ".")
        levels = []
        for separator in separators:
            num = separator[0]
            char = separator[1]
            last_num = last_separator[0]
            last_char = last_separator[1]
            extract = False

            if (last_char == "." and char in ".[") or (last_char == "[" and char == "]"):
                extract = True

            #after any closing brackets we expect . or another bracket
            elif last_char == "]" and char in  ".[":
                if num - last_num > 1:
                    raise SyntaxError("Expected . or [ in entry parameter at pos {}"
                                      .format(last_num + 1))

            elif (last_char != "[" and char == "]") or (last_char == "[" and char in ".["):
                raise SyntaxError("Unexpeced {} in entry parameter at pos {}".format(char, num))

            if extract:
                # Something between separators
                if num - last_num > 1:
                    is_array = (last_char == "[")
                    ident = entry[last_num + 1:num]
                    if is_array:
                        if not (ident).isdigit():
                            raise SyntaxError("Expecting integer in entry "
                                              "parameter at pos {} got '{}' instead"
                                              .format(last_num, ident))
                        ident = int(ident)

                    if isinstance(ident, str):
                        ident = ident.replace("\\[", "[").replace("\\]", "]").replace("\\.", ".")

                    levels.append((is_array, ident))
                elif (last_num == -1 and char == "["):
                    pass
                else:
                    raise SyntaxError("Expecting string at pos {} not {}".format(num, char))
            last_separator = separator
        return levels




def get_variable_entry(variable, entry, get_parent=False):
    """
    Returns the entry in the file if present
    if not present it returns None
    """
    current = variable
    keyinfo = EntryParser().parse(entry)

    # If empty
    if not keyinfo:
        if get_parent:
            return dict({'root': current}), (False, "root")
        return current

    for isindex, key in keyinfo[:-1 if get_parent else None]:
        if isindex:
            key = int(key)

        if isinstance(current, (list, dict)) and \
           ((isindex and key < len(current)) or (key in current)):
            current = current[key]
        else:
            return None

    if get_parent:
        var = current
        info = keyinfo[-1]
        isindex = info[0]
        key = info[1]

        is_valid_list = isinstance(var, list) and (isindex and key < len(current))
        is_valid_dict = isinstance(var, dict) and ((not isindex) and (key in var))

        if not is_valid_list and not is_valid_dict:
            return None
        return var, info
    return current

def create_variable_entry(variable, entry, value, force):
    """
    Creates the entry with the value in the variable
    """

    current = variable

    keyinfo = EntryParser().parse(entry)

    # If empty
    if not keyinfo:
        current = value

    cur_entry = "" #Just used for error output
    for num, (isindex, key) in enumerate(keyinfo[:-1]):
        is_array = keyinfo[num + 1]

        if (key in current) or (isindex and key < len(current)):
            current = current[key]
        elif isindex:
            raise TypeError("List {}[{}]: List entries can not be created!".format(cur_entry, key))
        else:
            if (current.get(key) is not None) and (not force):
                raise TypeError("Can not overwrite {} "
                                "cause of exitsting different type,"
                                "set force to true to do so".format(cur_entry))
            current[key] = {}
            current = current[key]

        cur_entry += "." + key if not is_array else "[{}]".format(key)

    current[keyinfo[-1][1]] = value

    return current, keyinfo[-1]

def processAction(variable, key, value, action):
    #TODO switch(action)
    #General
    if action == "set":
        variable[key] = value

    #List
    elif action == "append":
        variable[key].append(value)

    elif action == "remove":
        variable[key].remove(value)

    elif action == "insert":
        variable.insert(key, value)

    elif action == "pop":
        variable[key].pop(int(value))

    elif action == "append_array":
        variable[key] += value


    #Math
    elif action == "add":
        variable[key] += value

    elif action == "subtract":
        variable[key] -= value

    elif action == "divide":
        variable[key] /= value

    elif action == "multiply":
        variable[key] *= value

    #Bool
    elif action == "and":
        variable[key] &= value

    elif action == "or":
        variable[key] |= value

    elif action == "nand":
        variable[key] = not bool(variable[key]) & bool(value)

    elif action == "nor":
        variable[key] = not bool(variable[key]) | bool(value)

    elif action == "xor":
        variable[key] = bool(value) != bool(variable[key])


def update_variable_entry(entry, # pylint: disable=too-many-arguments
                          variable,
                          action="set",
                          value=None,
                          var_type=None,
                          create=False,
                          force=False):
    """
    Sets a entry in a variable to a specified value
    """
    general_actions = [
        "set",
    ]

    math_actions = [
        "add",
        "subtract",
        "divide",
        "multiply"
    ]

    bool_actions = [
        "and"
        "or"
        "nand"
        "nor"
        "xor"
    ]

    list_actions = [
        "append_array",
        "append",
        "insert",
        "remove",
        "pop"
    ]
    value_types = ["None", "str", "bool", "int", "float", "dict", "list"]

    all_actions = general_actions + math_actions + list_actions + bool_actions

    if action not in all_actions:
        raise ValueError("Action {} is not supported!".format(action))

    keyinfo = get_variable_entry(variable, entry, True)

    if create and action != "set":
        raise ValueError("create only supported for set action")

    if keyinfo is None:
        if create:
            keyinfo = create_variable_entry(variable, entry, value, force)
            return
        else:
            raise KeyError("Entry not found!")

    variable = keyinfo[0]
    isindex = keyinfo[1][0]
    key = keyinfo[1][1]

    if str(var_type) in value_types:
        if not str(var_type) == "None":
            value = eval(var_type + "({})".format(str(value)))
    else:
        raise ValueError("var_type must be of one of {}.".format(str(value_types)))

    action_unsuitable = (action in list_actions and not isinstance(variable[key], (list)))\
        or (action in math_actions and not isinstance(variable[key], (int, float)))

    if action_unsuitable:
        raise ValueError("Action {} not suitable to entry {} with type {}".format(
            action, entry, variable[key].__class__.__name__))

    if (key in variable) or (isindex and key < len(variable)):
        processAction(variable, key, value, action)


class ActionModule(ActionBase):
    """
    Action Module for var_manip
    """
    TRANSFERS_FILES = False

    def run(self, tmp=None, task_vars=None):
        if task_vars is None:
            task_vars = dict()

        result = super(ActionModule, self).run(tmp, task_vars)

        facts = dict()
        var_name = self._task.args.pop('var')

        var_name = self._templar.template(var_name)
        if not isidentifier(var_name):
            result['failed'] = True
            result['msg'] = ("The variable name '%s' is not valid. Variables must start with"
                             "a letter or underscore character, and contain only "
                             "letters, numbers and underscores." % var_name)
            return result

        init = self._task.args.pop('init', None)
        value = self._task.args.pop('value', None)
        changes = self._task.args.pop('changes', [])

        var = None

        if value:
            if changes or init:
                result['failed'] = True
                result['msg'] = ("value can not be set with init or changes")
                return result
            var = value

        if not var:
            var = task_vars.pop(var_name, init)

        if var is None:
            result['failed'] = True
            result['msg'] = ("Variable '%s' does not exist")
            return result


        for change in changes:

            value = change.pop("value")
            if not C.DEFAULT_JINJA2_NATIVE and isinstance(value, string_types) and \
               value.lower() in ('true', 'false', 'yes', 'no'):
                value = boolean(value, strict=False)

            update_variable_entry(change.pop("entry", ""),
                                  var,
                                  change.pop("action", "set"),
                                  value,
                                  change.pop("var_type", None),
                                  change.pop("create", False),
                                  change.pop("force", False))

        facts[var_name] = var
        result['changed'] = False
        result['ansible_facts'] = facts
        result['_ansible_facts_cacheable'] = boolean(self._task.args.pop('cacheable', False))
        return result
